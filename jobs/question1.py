from mrjob.job import MRJob
from mrjob.step import MRStep
import xml.etree.ElementTree as ET
import ast

class question1(MRJob):
    SORT_VALUES = True
    
    def steps(self):
        return [
            MRStep(mapper=self.mapper1, reducer=self.reducer1),
            MRStep(mapper=self.mapper2, reducer=self.reducer2),
            MRStep(reducer=self.top10),

        ]

    def mapper1(self, _, line):
        if "row" in line:
            root = ET.fromstring(line);
            # posts.xml File
            if "PostTypeId" in line:
                try:
                    postId = root.attrib['Id']
                    owneruserId = root.attrib['OwnerUserId']
                    yield ['PostId', postId], ['A', 'UserId', owneruserId]
                except Exception as e: # some posts don't have owner user Id
                    pass
            # votes.xml File
            elif "VoteTypeId" in line:
                postId = root.attrib['PostId']
                voteTypeId = int(root.attrib['VoteTypeId'])
                # Testing small sample file, known values where 7 is the flagged ones and should be User13(2) User20(1)
                #if voteTypeId == 7:
                # Large files
                if voteTypeId == 4 or voteTypeId == 10 or voteTypeId == 12 or voteTypeId == 15:
                    yield ['PostId', postId], ['B', 'Flagged']
                else:
                    pass
            # users.xml File
            elif "Reputation" in line:
                userId = root.attrib['Id']
                displayName = root.attrib['DisplayName']
                yield ['A', 'UserId', userId], ['DisplayName', displayName]

            # other file
            else:
                pass
        else:
            pass


    def reducer1(self, key, values):
        values = [x for x in values]
        # hit
        if len(values) > 1:
            if values.__contains__(['B', 'Flagged']):
                if values[0][0] == 'A':
                    yield values[0], ['B', 'Flagged']

        if key[1] == 'UserId':
            yield key, values[0]


    def mapper2(self, key, line):
        values = [x for x in line]
        yield key, values

    def reducer2(self, key, values):
        values = [x for x in values]
        flaggedCount = values.count(['B', 'Flagged'])
        username = [x for x in values if x[0] == 'DisplayName']
        displayName = 'Invalid Username'
        if len(username) > 0:
            displayName = username[0][1]
        try:
            # None, ['UserId', UserId, 'TODO DisplayName', count]
            yield None, [key[1], key[2], displayName, int(flaggedCount)]
        except Exception as e:
            print(e)
            x = input('Pause Exception')

    def top10(self, key, values):
        #z = sorted(values, key = lambda x: int(x[0]))[-10:]
        values = [x for x in values]
        z = sorted(values, key = lambda x: int(x[3]))[-10:]
        for item in z:
            yield [item[0], item[1], item[2]], ['Flagged Count', item[3]]
