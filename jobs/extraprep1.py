from mrjob.job import MRJob
from mrjob.step import MRStep
import xml.etree.ElementTree as ET

class extraprep1(MRJob):
    SORT_VALUES = True

    def steps(self):
        return [
            MRStep(mapper=self.mapper1, reducer=self.reducer1)
            ,MRStep(mapper=self.mapper2, reducer=self.reducer2)
            ,MRStep(reducer=self.top10)
        ]

    def mapper1(self, _, line):
        if "row" in line:
            root = ET.fromstring(line);
            # users.xml file
            if "Reputation" in line:

                userId = root.attrib['Id']
                reputation = root.attrib['Reputation']
                username = root.attrib['DisplayName']
                yield ['UserId', userId], ['A', 'Reputation', reputation, 'DisplayName', username]

            # badges.xml File
            elif "TagBased" in line:
                userId = root.attrib['UserId']
                name = root.attrib['Name']
                classType = int(root.attrib['Class'])
                badgeType = 'Gold'
                if classType == 3:
                    badgeType = 'Bronze'
                if classType == 2:
                    badgeType = 'Silver'

                yield ['UserId', userId], ['B', badgeType]

            # other file ignore
            else:
                pass
        else:
            pass

    def reducer1(self, key, values):
        values = [x for x in values]
        # hit, join hits where users and badge file mapper returns (key, value) = (['UserId', x], ....)
        if len(values) > 1:
            yield key, values

    def mapper2(self, key, line):
        # check for correct format from previous step

        if line[0][0] == 'A':
            # create (key, value) pair ['UserId', x, 'UserName', y],[ count of number of badges the user has]
            gold = line.count(['B', 'Gold'])
            silver = line.count(['B', 'Silver'])
            bronze = line.count(['B', 'Bronze'])
            yield ['UserId: ' + str(key[1]), 'UserName: ' + line[0][4]], [gold, silver, bronze]
        pass

    def reducer2(self, key, values):
        values = [x for x in values]
        gold = 0
        silver = 0
        bronze = 0
        for value in values:
            gold += value[0]
            silver += value[1]
            bronze += value[2]

        # sum all users and badge counts from the various mapper steps
        yield None, [key, gold, silver, bronze]

    def top10(self, key, values):
        values = [x for x in values]

        # get top 10
        z = sorted(values, key=lambda x: int(x[1]))[-10:]

        for item in z:
            total = item[1] + item[2] + item[3]
            yield item[0], ['Total', total, 'Gold', item[1], 'Silver', item[2], 'Bronze', item[3]]
