from mrjob.job import MRJob
from mrjob.step import MRStep

import xml.etree.ElementTree as ET

class question2(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper1, reducer=self.reducer1)
            ,MRStep(mapper=self.mapper2, reducer=self.reducer2)
                ]

    def mapper1(self, _, line):
        if "row" in line:
            root = ET.fromstring(line);
            # posts.xml File
            if "PostTypeId" in line:
                postId = root.attrib['Id']
                body = root.attrib['Body']
                if 'newton' in body.lower():
                    if 'motion' in body.lower() or 'first' in body.lower() or 'second' in body.lower() or 'third' in body.lower():
                        yield postId, ['A', 1]
                yield postId, 1
                yield 'Total', 1

            # comments.xml
            if "Text" in line:
                try:
                    postId = root.attrib['PostId']
                    commentId = root.attrib['Id']
                    body = root.attrib['Text']

                    if 'newton' in body.lower():
                        if 'motion' in body.lower() or 'first' in body.lower() or 'second' in body.lower() or 'third' in body.lower():
                            yield postId, ['B', 1]
                except Exception as e:
                    pass


    def reducer1(self, key, values):
        values = [x for x in values]

        if len(values) > 1 and str(key) != 'Total':
            yield 'Newton Posts', 1

        if str(key) == 'Total':
            yield 'Total Posts', sum(values)

    def mapper2(self, key, values):
        yield key, values

    def reducer2(self, key, values):
        values = [x for x in values]
        yield key, sum(values)

# time 47s
# Newton Posts (a): 6545
# Total Posts (b): 258445
# Percentage Newton: a/b = 2.54