#Newton's Law of Motion: % posts
#Result: 4.5%

from mrjob.job import MRJob
from mrjob.step import MRStep
import xml.etree.ElementTree as ET
import time as time
import ast

start_time = time.time()  

class newton(MRJob):
    SORT_VALUES = True

    def steps(self):
        return [MRStep(mapper=self.mapper1, reducer=self.reducer1),
                MRStep(mapper=self.mapper2, reducer=self.reducer2)]

    def mapper1(self, _, line):
        if "row" in line: #otherwise heading or other unwanted line
            root = ET.fromstring(line);
            # posts.xml File
            if "PostTypeId" in line:
                postId = root.attrib['Id']
                relevant = 0
                if ('newton' in line or 'Newton' in line or 'nweton' in line or 'Nweton' in line 
                    or 'newtno' in line or 'Newtno' in line or 'nwton' in line or 'Nwton' in line
                    or 'newtn' in line or 'Newtn' in line or 'NEWTON' in line):
                    if ('law' in line or 'lw' in line or 'motion' in line or 'move' in line
                        or 'mtion' in line or 'Law' in line or 'Motion' in line or 'LAW' in line
                        or 'MOTION' in line):
                            relevant = 1
                    else:
                        pass
                else:
                    pass
                
                yield postId, ['aPost', relevant, 1]
            # Comments.xml File
            elif "Text" in line:
                try:
                    postId = root.attrib['PostId']
                    if ('newton' in line or 'Newton' in line or 'nweton' in line or 'Nweton' in line 
                        or 'newtno' in line or 'Newtno' in line or 'nwton' in line or 'Nwton' in line
                        or 'newtn' in line or 'Newtn' in line or 'NEWTON' in line):
                        if ('law' in line or 'lw' in line or 'motion' in line or 'move' in line
                            or 'mtion' in line or 'Law' in line or 'Motion' in line or 'LAW' in line
                            or 'MOTION' in line):
                                yield postId, ['Comment', 1]
                        else:
                            pass
                    else:
                        pass
                except Exception as e: # some comments don't have post Id
                    pass
            # other file
            else:
                pass
        else:
            pass

    def reducer1(self, key, values):
        values = [x for x in values]
        yield key, str(values)
    
    def mapper2(self, _, line):
        #line = [['aPost', 1, 1]]   ~OR~
        #line = [['aPost', 0, 1], ['Comment', 1], ['Comment', 1], ['Comment', 1]]
        line = ast.literal_eval(line) # line[0] aPost, line [1] Comment
        relevant = float(line[0][1])
        if len(line) > 1: #Comments hit search term
            relevant = 1.0
        count = float(line[0][2])    
        yield relevant, count

    def reducer2(self, key, values):
        values = [x for x in values]
        x = sum(values)
        yield key, x

if __name__ == '__main__':
  newton.run()
  
duration = time.time() - start_time
print("Time:", round(duration,0), "seconds /", round(duration / 60,2), "minutes") 
#1.92 minutes on full dataset

