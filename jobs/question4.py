from mrjob.job import MRJob
from mrjob.step import MRStep
import xml.etree.ElementTree as ET

class question4(MRJob):


    def steps(self):
        return [
            MRStep(mapper=self.mapper1, reducer=self.reducer1)
            ,MRStep(mapper=self.mapper2, reducer=self.reducer2)
            ,MRStep(reducer=self.top20)

        ]

    def mapper1(self, _, line):
        if "row" in line:
            root = ET.fromstring(line);
            # posts.xml File
            if "PostTypeId" in line:
                try:
                    creationDate = root.attrib['CreationDate']
                    userId = root.attrib['OwnerUserId']
                    yield userId, ['B', 1]
                except Exception as e:
                    # print('Exception', e, line)
                    # happens when PostTypeId = 2 therefore no OwnerUserId
                    pass

            # users.xml
            elif "Reputation" in line:
                try:
                    country_lookup = ['Afghanistan', 'Åland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra',
                                      'Angola', 'Anguilla', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia',
                                      'Aruba', 'Australia', 'Austria',
                                      'Azerbaijan', 'Bahrain', 'Bahamas', 'Bangladesh', 'Barbados', 'Belarus',
                                      'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan',
                                      'Bolivia, Plurinational State of', 'Bonaire, Sint Eustatius and Saba',
                                      'Bosnia and Herzegovina', 'Botswana',
                                      'Bouvet Island', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam',
                                      'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada',
                                      'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad',
                                      'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia',
                                      'Comoros', 'Congo', 'Congo, the Democratic Republic of the', 'Cook Islands',
                                      'Costa Rica', 'Côte d\'Ivoire', 'Croatia',
                                      'Cuba', 'Curaçao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica',
                                      'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea',
                                      'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands (Malvinas)', 'Faroe Islands',
                                      'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia',
                                      'French Southern Territories', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana',
                                      'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala',
                                      'Guernsey', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti',
                                      'Heard Island and McDonald Islands',
                                      'Holy See (Vatican City State)', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland',
                                      'India', 'Indonesia', 'Iran, Islamic Republic of', 'Iraq', 'Ireland',
                                      'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey',
                                      'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati',
                                      'Korea, Democratic People\'s Republic of', 'Korea, Republic of', 'Kuwait',
                                      'Kyrgyzstan', 'Lao People\'s Democratic Republic', 'Latvia', 'Lebanon', 'Lesotho',
                                      'Liberia',
                                      'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macao',
                                      'Macedonia, the Former Yugoslav Republic of', 'Madagascar', 'Malawi', 'Malaysia',
                                      'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania',
                                      'Mauritius',
                                      'Mayotte', 'Mexico', 'Micronesia, Federated States of', 'Moldova, Republic of',
                                      'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique',
                                      'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia',
                                      'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island',
                                      'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau',
                                      'Palestine, State of', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru',
                                      'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar',
                                      'Réunion', 'Romania', 'Russian Federation', 'Rwanda', 'Saint Barthélemy',
                                      'Saint Helena, Ascension and Tristan da Cunha', 'Saint Kitts and Nevis',
                                      'Saint Lucia', 'Saint Martin (French part)', 'Saint Pierre and Miquelon',
                                      'Saint Vincent and the Grenadines', 'Samoa', 'San Marino',
                                      'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles',
                                      'Sierra Leone', 'Singapore',
                                      'Sint Maarten (Dutch part)', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia',
                                      'South Africa', 'South Georgia and the South Sandwich Islands', 'South Sudan',
                                      'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland',
                                      'Sweden', 'Switzerland', 'Syrian Arab Republic', 'Taiwan, Province of China',
                                      'Tajikistan', 'Tanzania, United Republic of', 'Thailand', 'Timor-Leste', 'Togo',
                                      'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey',
                                      'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine',
                                      'United Arab Emirates', 'United Kingdom', 'United States',
                                      'United States Minor Outlying Islands', 'Uruguay', 'Uzbekistan', 'Vanuatu',
                                      'Venezuela, Bolivarian Republic of',
                                      'Viet Nam', 'Virgin Islands, British', 'Virgin Islands, U.S.',
                                      'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe',
                                      ]

                    location = root.attrib['Location']
                    userId = root.attrib['Id']
                    # NB this lookup is just for people who have correct spelling and format is sometimes something like City, Province, Country
                    # It does not pickup any other countries where an additional condition below this for could be used
                    foundCountry = False
                    for country in country_lookup:
                        # .lower converts the lookup and location to lower that don't have to account for funny cases
                        if country.lower() in location.lower():
                            foundCountry = True
                            yield userId, ['A', country]
                    ''' Trying to catch some funny cases '''
                    '''
                    if foundCountry == False:
                        # Try some other conditions
                        if 'Buffalo' in location:
                            yield userId, ['A', 'United States']
                    '''

                except Exception as e:
                    pass
        pass

    def reducer1(self, key, values):
        values = [x for x in values]

        if len(values) > 1:
            if values[0][0] == 'A':
                # how many times user has posted
                count = values.count(['B', 1])
                yield values[0][1], count

    def mapper2(self, key, values):
        # Country, Posts
        yield key, int(values)
        #yield key, sum(values)

    def reducer2(self, key, values):
        values = [x for x in values]
        yield None, [key, sum(values)]

    def top20(self, key, values):
        values = [x for x in values]

        z = sorted(values, key=lambda x: int(x[1]))[-20:]
        for item in z:
            # Country Name, Count
            yield item[0], item[1]

''' RESULTS
time = 28seconds 

United Kingdom	11266
India	9893
United States	7804
Germany	5673
Greece	3770
Canada	3360
Czech Republic	2799
Spain	2794
Australia	2733
France	2705
Italy	1575
Netherlands	1396
Austria	1079
Denmark	994
Switzerland	909
Belgium	712
Brazil	687
Sweden	557
Argentina	530
Poland	515

'''