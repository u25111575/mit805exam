from mrjob.job import MRJob
from mrjob.step import MRStep
import datetime
from dateutil import parser
import xml.etree.ElementTree as ET

class question3(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper1, reducer=self.reducer1)
            ,MRStep(mapper=self.mapper2, reducer=self.reducer2)


        ]

    '''
    You should use the upmod in the votes XML, get the post ID from the vote and then do a lookup in the posts XML with 
    the ID. The creation date in the posts XML has a time. You should then calculate the average number of votes 
    (upmods) entries per post for each hour of the day. You can completely ignore users from this part. It is the 
    average number of votes per post, irrespective of which user cast them.
    '''
    def week_lookup(self, weekday):
        if weekday == 0:
            return "Monday"
        if weekday == 1:
            return "Tuesday"
        if weekday == 2:
            return "Wednesday"
        if weekday == 3:
            return "Thursday"
        if weekday == 4:
            return "Friday"
        if weekday == 5:
            return "Saturday"
        return "Sunday"

    def mapper1(self, _, line):
        if "row" in line:
            root = ET.fromstring(line);
            # posts.xml File
            if "PostTypeId" in line:
                postId = root.attrib['Id']
                creationDate = root.attrib['CreationDate']
                creationDate = parser.parse(creationDate)

                weekday = self.week_lookup(creationDate.weekday())
                hour = creationDate.time().hour

                yield postId, ['A', weekday, hour]

            # votes.xml
            if "VoteTypeId" in line:
                postId = root.attrib['PostId']
                voteTypeId = int(root.attrib['VoteTypeId'])
                # Upvote = 2
                if voteTypeId == 2:
                    yield postId, ['B', 1]

    def reducer1(self, key, values):
        values = [x for x in values]

        if len(values) > 1:
            if values[0][0] == "A":
                count = values.count(['B', 1])
                yield [values[0][1], values[0][2]], ['B', 'Upvotes', int(count)]

        if values[0][0] == "A":
            yield [values[0][1], values[0][2]], ['A', 'Posts', 1]

    def mapper2(self, key, values):
        values = [x for x in values]
        yield key, values

    def reducer2(self, key, values):
        values = [x for x in values]
        if len(values) > 1:
            voteCount = 0
            for vote in values:
                if vote[0] == 'B':
                    voteCount += int(vote[2])

            postTotal = values.count(['A', 'Posts', 1])

            #yield [str(key[1]) + ":00 " + str(key[0])], ['Total', postTotal, 'Upmods', voteCount, 'Average', round(voteCount/postTotal,3)]
            yield [str(key[1]), str(key[0])], ['Average', round(voteCount / postTotal, 3)]