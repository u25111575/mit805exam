from mrjob.job import MRJob
from mrjob.step import MRStep
import xml.etree.ElementTree as ET
import ast

class FindEntries():
    '''
    Function used to quickly loop through a given file listing all elements containing the value
    '''
    def find_x_in_file_y(self, filename, element, value, contains = True):
        try:
            f = open('input\\' + filename)
            line = f.readline()
        except Exception as e:
            print('No such file:', filename)
            return

        while line is not None:
            line = f.readline()
            line = line.replace('\n', '')

            if len(line) == 0:
                break
            try:

                root = ET.fromstring(line);
                if contains:
                    if(str(value) in root.attrib[element]):
                        print(root.attrib)
                else:
                    if (root.attrib[element] == str(value)):
                        print(root.attrib)
                        return

            except Exception as e:
                #print(e)
                continue
    def print_first10_x_in_file(self, filename, element):
        try:
            f = open('input\\' + filename)
            line = f.readline()
        except Exception as e:
            print('No such file:', filename)
            return
        counter = 0
        while line is not None:
            line = f.readline()
            line = line.replace('\n', '')

            if len(line) == 0:
                break
            try:

                root = ET.fromstring(line)
                print(element, ":", root.attrib[element])
                counter += 1

                if counter > 10:
                    break

            except Exception as e:
                #print(e)
                continue