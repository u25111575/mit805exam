from mrjob.job import MRJob
from mrjob.step import MRStep
from jobs.question1 import question1
from jobs.question2 import question2
from jobs.question3 import question3
from jobs.question4 import question4
from jobs.extraprep1 import extraprep1

import time

from helper.find_entries import FindEntries

if __name__ == '__main__':

    menu = "\n *** MIT805 Exam ***" \
           "\n 1. Question 1" \
           "\n 2. Question 2" \
           "\n 3. Question 3" \
           "\n 4. Question 4" \
           "\n 5. TODO" \
           "\n 6. Extra Prep1 - Count User Badges list top 20" \
           "\n t. Find Element in File" \
           "\n q. QUIT"

    while True:
        print(menu)
        user_input = input("Choice: ")

        if user_input == "1":
            question1.run()
        elif user_input == "2":
            start_time = time.time()
            question2.run()
            duration = time.time() - start_time
            print("Time:", round(duration, 0), "seconds")
        elif user_input == "3":
            start_time = time.time()
            question3.run()
            duration = time.time() - start_time
            print("Time:", round(duration, 0), "seconds")
        elif user_input == "4":
            start_time = time.time()
            question4.run()
            duration = time.time() - start_time
            print("Time:", round(duration, 0), "seconds")
        elif user_input == "6":
            extraprep1.run()
        elif user_input == "t":
            FindEntries.find_x_in_file_y(None, 'posts.xml', 'Id', 1)
        elif user_input == "s":
            FindEntries.print_first10_x_in_file(None, 'posts.xml', 'ParentId')
        elif user_input == "q":
            break

        else:
            continue
