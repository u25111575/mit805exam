clean:
	del output\*.txt

sample:
	py -3 main.py input\small\users.xml input\small\badges.xml -o output\

run:
	py -3 main.py input\big\users.xml input\big\badges.xml -o output\